//Johanna Luangxay (1939023)
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	
	private TextField message;
	private TextField win;
	private TextField loss;
	private TextField tie;
	private String playerChoice;
	private RpsGame game;
	
	public RpsChoice (TextField message, TextField win, TextField loss, TextField tie, String playerChoice, RpsGame game)
	{
		this.message = message;
		this.win = win;
		this.loss = loss;
		this.tie = tie;
		this.playerChoice = playerChoice;
		this.game = game;
	}

	@Override
	public void handle(ActionEvent event) {
		String result = game.playRound(playerChoice);
		message.setText(result);
		win.setText("Wins: "+game.getWin());
		loss.setText("Losses: "+game.getLoss());
		tie.setText("Ties: "+game.getTie());
	}
	
	
}
