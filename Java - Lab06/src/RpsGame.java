//Johanna Luangxay (1939023)
import java.util.Random;

public class RpsGame {
	private int win = 0;
	private int tie = 0;
	private int loss = 0;
	Random rand = new Random();
	
	public int getWin()
	{
		return win;
	}
	public int getTie()
	{
		return tie;
	}
	public int getLoss()
	{
		return loss;
	}
	
	public String playRound(String playerChoice)
	{
		playerChoice = playerChoice.toLowerCase();
		int randNum = rand.nextInt(3);
		String pcChoice = "";
		String result = "";
		
		switch (randNum)
		{
		case 0:
			pcChoice = "rock";
			break;
		case 1:
			pcChoice = "paper";
			break;
		case 2:
			pcChoice = "scissors";
			break;
		}
		
		if(pcChoice.equals(playerChoice))
		{
			result = "Computer plays "+pcChoice+", so it's a tie";
			tie++;
		}
		else if (pcChoice.equals("rock") && playerChoice.equals("scissors") || 
				pcChoice.equals("paper") && playerChoice.equals("rock") || 
				pcChoice.equals("scissors") && playerChoice.equals("paper"))
		{
			result = "Computer plays "+pcChoice+", so Computer won";
			loss++;
		}
		else
		{
			result = "Computer plays "+pcChoice+", so Player won";
			win++;
		}
		
		return result;	
	}
}
