//Johanna Luangxay (1939023)
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	
	
	private RpsGame game = new RpsGame();
	
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 950, 300); 
		scene.setFill(Color.BLACK);
		
		//Button Row 1 creation
		HBox buttons = new HBox();
		Button rockBtn = new Button("Rock");
		Button paperBtn = new Button("Paper");
		Button scissorsBtn = new Button("Scissors");
		buttons.getChildren().addAll(rockBtn,paperBtn,scissorsBtn);
		
		//Text Fields Row 2 creation
		HBox textFields = new HBox();
		TextField welcomeTF = new TextField("Welcome!");
		welcomeTF.setPrefWidth(350);
		TextField winTF = new TextField("Wins: "+game.getWin());
		winTF.setPrefWidth(200);
		TextField lossTF = new TextField("Losses: "+game.getLoss());
		lossTF.setPrefWidth(200);
		TextField tieTF = new TextField("Ties: "+game.getTie());
		tieTF.setPrefWidth(200);
		textFields.getChildren().addAll(welcomeTF, winTF, lossTF, tieTF);
		
		//Events
		RpsChoice rockEvent = new RpsChoice
				(welcomeTF,winTF,lossTF,tieTF,"rock",game);
		rockBtn.setOnAction(rockEvent);
		RpsChoice paperEvent = new RpsChoice
				(welcomeTF,winTF,lossTF,tieTF,"paper",game);
		paperBtn.setOnAction(paperEvent);
		RpsChoice scissorsEvent = new RpsChoice
				(welcomeTF,winTF,lossTF,tieTF,"scissors",game);
		scissorsBtn.setOnAction(scissorsEvent);
		
		//To make row 1 and row 2 in a column
		VBox overall = new VBox();
		overall.getChildren().addAll(buttons,textFields);
		
		//Added to the root
		root.getChildren().addAll(overall);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
